substitutions:
  # pinout
  PE_DAT: GPIO8
  PE_CLK: GPIO2
  PE_LATCH: GPIO1
  PE_OE: GPIO0
  # BOOT_PIN: GPIO9
  INT_LED_PIN: GPIO10
  CAN_RX: GPIO7
  CAN_TX: GPIO6
  FLOW_CNT: GPIO3
  RAIN_CNT: GPIO4 
  RLY_OUT: GPIO5
  # default values
  device_name: "Irrigation Controller"
  name: "irrigationctrl"
  puls_per_liter: "450.0"
  max_zone_duration: "1000"

# Enable Home Assistant API
api:
  encryption:
    key: "change_me"  # get an own one from https://esphome.io/components/api.html
  actions:
    - action: set_multiplier
      variables:
        multiplier: float
      then:
        - sprinkler.set_multiplier:
            id: sprinkler_ctrl
            multiplier: !lambda 'return multiplier;'
    - action: set_repeat
      variables: 
        repeat: int
      then:
        - sprinkler.set_repeat:
            id: sprinkler_ctrl
            repeat: repeat
    - action: start_full_cycle
      then:
        - sprinkler.start_full_cycle: sprinkler_ctrl
    - action: start_single_valve
      variables:
        valve: int
      then:
        - sprinkler.start_single_valve:
            id: sprinkler_ctrl
            valve_number: !lambda 'return valve;'
    - action: next_valve
      then:
        - sprinkler.next_valve: sprinkler_ctrl
    - action: previous_valve
      then:
        - sprinkler.previous_valve: sprinkler_ctrl
    - action: shutdown
      then:
        - sprinkler.shutdown: sprinkler_ctrl

ota:
  - platform: esphome
    password: "change_me" # get an own one from f.e. https://www.avast.com/random-password-generator#pc

wifi:
  networks:
    ssid: !secret wifi_ssid
    password: !secret wifi_password

esphome:
  platformio_options:
      board_build.flash_mode: dio
      # board_build.f_flash: 40000000L
      # board_build.flash_size: 4MB
  name: ${name}
  friendly_name: ${device_name}
  # This will allow for (future) project identification,
  # configuration and updates.
  project:
    name: irrigation-ctrl.project-template
    version: "1.0"

# This should point to the public location of this yaml file.
dashboard_import:
  package_import_url: gitlab://laszloh/irrigation-controller/SW/esphome/irrigation-controller.yaml@main
  import_full_config: true

esp32:
  variant: ESP32C3
  board: esp32-c3-devkitm-1
  framework:
    type: esp-idf
    version: recommended
    # platform_version: 6.7.0
    # Custom sdkconfig options
    sdkconfig_options:
      CONFIG_COMPILER_OPTIMIZATION_SIZE: y
    # Advanced tweaking options
    advanced:
      ignore_efuse_mac_crc: false

# Enable logging
logger:
  level: VERBOSE
  # hardware_uart: USB_CDC

web_server:
  port: 80

text_sensor:
  # ESPHome version string
  - platform: version
    name:  ESPHome Version
  # fancy wifi information
  - platform: wifi_info
    ip_address:
      name: IP
    ssid:
      name: SSID
    bssid:
      name: BSSID

sensor:
  # board uptime
  - platform: uptime
    name: Uptime
  # WiFi signal information
  - platform: wifi_signal
    name: WiFi Signal
    update_interval: 60s
  # Flow meter
  - platform: pulse_counter
    pin: $FLOW_CNT
    name: "Flow Meter Pulse Counter"
    update_interval: 5s
    id: flow_counter
  - platform: template
    name: "Flow Meter"
    lambda: |-
      return (id(flow_counter).state / 450.0) * 60.0;
    update_interval: 5s
    unit_of_measurement: "L/hr"
  # Rain measurment counter
  - platform: pulse_counter
    pin: $RAIN_CNT
    name: "Rain Volume Pulse Counter"
    update_interval: 5s
    id: rain_counter
  - platform: template
    name: "Rain Volume Meter"
    lambda: |-
      return (id(rain_counter).state / 450.0) * 60.0;
    update_interval: 5s
    unit_of_measurement: "L/hr"

sprinkler:
  - id: sprinkler_ctrl
    main_switch: "Sprinklers"
    auto_advance_switch: "Sprinklers auto advance"
    valve_overlap: 3s
    pump_start_valve_delay: 10s
    pump_stop_pump_delay: 10s
    repeat_number:
      id: sprinkler_repeat
      name: "Sprinkler Repeat"
      initial_value: 1
      min_value: 1
      max_value: 100
    multiplier_number:
      id: sprinkler_multiplier
      name: "Sprinkler Multiplier"
      initial_value: 1.0
      min_value: 0.1
      max_value: 5
    valves: 
      - valve_switch: "Zone 1"
        enable_switch: "Enable Zone 1"
        pump_switch_id: main_pump
        run_duration_number: 
          id: zone1_run_duration
          name: "Zone 1 run time"
          initial_value: 100
          min_value: 0
          max_value: ${max_zone_duration}
          unit_of_measurement: s
        valve_switch_id: valve1
      - valve_switch: "Zone 2"
        enable_switch: "Enable Zone 2"
        pump_switch_id: main_pump
        run_duration_number: 
          id: zone2_run_duration
          name: "Zone 2 run time"
          initial_value: 100
          min_value: 0
          max_value: ${max_zone_duration}
          unit_of_measurement: s
        valve_switch_id: valve2
      - valve_switch: "Zone 3"
        enable_switch: "Enable Zone 3"
        pump_switch_id: main_pump
        run_duration_number: 
          id: zone3_run_duration
          name: "Zone 3 run time"
          initial_value: 100
          min_value: 0
          max_value: ${max_zone_duration}
          unit_of_measurement: s
        valve_switch_id: valve3
      - valve_switch: "Zone 4"
        enable_switch: "Enable Zone 4"
        pump_switch_id: main_pump
        run_duration_number: 
          id: zone4_run_duration
          name: "Zone 4 run time"
          initial_value: 100
          min_value: 0
          max_value: ${max_zone_duration}
          unit_of_measurement: s
        valve_switch_id: valve4
      - valve_switch: "Zone 5"
        enable_switch: "Enable Zone 5"
        pump_switch_id: main_pump
        run_duration_number: 
          id: zone5_run_duration
          name: "Zone 5 run time"
          initial_value: 100
          min_value: 0
          max_value: ${max_zone_duration}
          unit_of_measurement: s
        valve_switch_id: valve5

sn74hc595:
  - id: "valve_hub"
    data_pin: $PE_DAT
    clock_pin: $PE_CLK
    latch_pin: $PE_LATCH
    oe_pin: $PE_OE

switch:
  - platform: gpio
    internal: True
    id: main_pump
    pin: $RLY_OUT
    restore_mode: ALWAYS_OFF
  - platform: gpio
    # name: "Valve 1"
    internal: True
    id: valve1
    pin:
      sn74hc595: "valve_hub"
      number: 0
      inverted: false
  - platform: gpio
    # name: "Valve 2"
    internal: True
    id: valve2
    pin:
      sn74hc595: "valve_hub"
      number: 1
      inverted: false
  - platform: gpio
    # name: "Valve 3"
    internal: True
    id: valve3
    pin:
      sn74hc595: "valve_hub"
      number: 2
      inverted: false
  - platform: gpio
    # name: "Valve 4"
    internal: True
    id: valve4
    pin:
      sn74hc595: "valve_hub"
      number: 3
      inverted: false
  - platform: gpio
    # name: "Valve 5"
    internal: True
    id: valve5
    pin:
      sn74hc595: "valve_hub"
      number: 4
      inverted: false
  - platform: gpio
    # name: "Valve 6"
    internal: True
    id: valve6
    pin:
      sn74hc595: "valve_hub"
      number: 5
      inverted: false
  - platform: gpio
    # name: "Valve 7"
    internal: True
    id: valve7
    pin:
      sn74hc595: "valve_hub"
      number: 6
      inverted: false
  - platform: gpio
    # name: "Valve 8"
    internal: True
    id: valve8
    pin:
      sn74hc595: "valve_hub"
      number: 7
      inverted: false

canbus:
  - platform: esp32_can
    tx_pin: $CAN_TX
    rx_pin: $CAN_RX
    id: can
    can_id: 4
    bit_rate: 125kbps
    on_frame:
      # echo inverted frames on id+1
      - can_id: 0
        can_id_mask: 0x00
        use_extended_id: false
        then:
          - lambda: |-
              std::string b(x.begin(), x.end());
              ESP_LOGD("CAN", "Packet received: 0x%04u %s", can_id , b.c_str());
              std::vector<uint8_t> data;
              data.reserve(x.size());
              for(const auto c : x) {
                data.push_back(~c);
              }
              id(can)->send_data(can_id + 1, false, data);
        # CAN controlled sprinkler
        #   commands are encoded into the canid, lowest 8 bits
        #   highest 2 bit is used to show direction:
        #     sender, receiver, 1 slave, 0 master
        #       00: invalid code: master to master
        #       01: master --> slave
        #       10: slave --> master
        #       11: slave --> slave
      - can_id:       0x08051000
        can_id_mask:  0x1FFFFF00
        use_extended_id: true
        then:
          - lambda: |-
              uint32_t cmd = can_id & 0xFF;
              auto sprinkler = id(sprinkler_ctrl);
              switch(cmd) {
                case 0:
                  // send back the status of the sprinkler
                  break;

                case 1: {
                    // set multiplier and repeat
                    //    multiplier: byte 0-3; float
                    //    repeat: byte 4; uint8_t
                    if(x.size() != 5) {
                      ESP_LOGE("CAN", "received sprinkler cmd 0 with %d bytes", x.size());
                      return;
                    }
                    float multiplier = *reinterpret_cast<float*>(&x[0]);
                    uint8_t repeat = x[4];
                    sprinkler->set_multiplier(multiplier);
                    sprinkler->set_repeat(repeat);
                    break;
                  }

                case 2:
                  // start a full cycle
                  sprinkler->start_full_cycle();
                  break;

                case 3: {
                    // start a single valve
                    //  valve number: byte 0; uint8_t
                    uint8_t valve = x[0];
                    sprinkler->start_single_valve(valve);
                    break;
                  }

                case 4:
                  // next valve
                  sprinkler->next_valve();
                  break;
                
                case 5:
                  // previous valve
                  sprinkler->previous_valve();
                  break;

                case 6: {
                    // shutdown sprinkler
                    // clear queue: byte 0, bool
                    bool clear = x[0];
                    sprinkler->shutdown(clear);
                    break;
                  }

                default:
                  break;
              }

status_led:
  pin: $INT_LED_PIN

time:
  - platform: homeassistant
    id: ha_time
  - platform: sntp
    id: sntp_time
    update_interval: 60min
